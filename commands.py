from .app import manager, db

@manager.command
def loaddb(filename):
	'''Rempli la base de données'''
	# chargement de notre jeu de données, l'encoding du fichier est spécifié comme étant de l'utf-8
	import yaml
	albums = yaml.load(open(filename, encoding="utf-8", errors="ignore"))

	# import des modèles
	from .models import Album, Genre
  	#creations de tous les albums et genres
	for album in albums:
		a = Album(by = album["by"],
			entryId = album["entryId"],
			img = album["img"],
			parent = album["parent"],
			releaseYear = album["releaseYear"],
			title = album["title"])
		g = album["genre"]
		for ge in g:
			genre = str(genre)
			if genre[-1] == ',' or genre[-1] == '.':
				genre = genre[:-1]
			genreAdd = ""
			for indiceLettre in range(len(genre)):
				if indiceLettre == 0 :
					genreAdd+=genre[indiceLettre].upper()
				elif indiceLettre !=0 and (genre[indiceLettre-1] ==" " or genre[indiceLettre-1] =="-"):
					genreAdd+=genre[indiceLettre].upper()
				elif genre[indiceLettre]=="[":
					break;
				else:
					genreAdd+=genre[indiceLettre]
			db.session.add(Genre(entryId = album["entryId"],genre = str(genreAdd)))
		db.session.add(a)
	'''Create the tables and populates them with data.'''

	# création de toutes les tables
	db.create_all()

	# chargement de notre jeu de données
	import yaml
	albums = yaml.load(open(filename))

	# import des modèles
	from .models import Album
  	#creations de tous les albums
	alb =  {}
	for ablum in albums:
		t = ablum["by"]
		if t not in alb:
			o = Album(title=t)
			db.session.add(o)
			alb[t] =  o
	db.session.commit()