from .app import db
class Album(db.Model):
	by  = db.Column(db.String(100))
	entryId = db.Column(db.Integer, primary_key = True)
	img = db.Column(db.String(200))
	parent = db.Column(db.String(100))
	releaseYear = db.Column(db.Integer)
	title = db.Column(db.String(150))
class Genre(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	entryId = db.Column(db.Integer, nullable = False)
	genre = db.Column(db.String(80), nullable = False)
	def __repr__(self):
		return "<Genre %s>" % (self.genre)
"""
return tous les genres disponibles dans la base de données
"""
def getGenres():
	from sqlalchemy import func
	g = db.session.query(func.lower(Genre.genre)).distinct(func.lower(Genre.genre)).all()
	r = sorted([genre[0] for genre in g])
	maj = []
	# pour remettres les majuscules aux genres
	for genre in r:
		new = ""
		if genre == "?": # il est pas beau
			continue
		for i in range(len(genre)):
			if i == 0 :
				new += genre[i].upper()
			elif genre[i-1] == " ":
				new += genre[i].upper()
			else:
				new += genre[i]
		maj.append(new)
	alphabet = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","1"]
	# on notera que dans alphabet il y a 1, l'astuce est de mettre 1 pour les types "autres" pour qu'ils soient les premiers une fois triés
	res={}
	for lettre in alphabet:
		res[lettre] = []
	for genre in maj:
		if len(genre) < 1:
			continue
		if genre[0] in res.keys():
			res[genre[0]].append(genre)
		else:
			res["1"].append(genre)
	return res
"""
return les artistes coresspondant à une chaine donnée
"""
def getArtistesByString(chaine):
	from sqlalchemy import desc
	a = db.session.query(Album.by).distinct(Album.by).order_by(desc(Album.by)).all()
	res=[]
	l = len(chaine)
	for artiste in a:
		if artiste[0] != None and artiste[0][0:l].lower() == chaine.lower():
			res.append(transformer(artiste[0]))
	return sorted(res)
"""
returns a tuple(img, album_title, band)
"""
def getAlbums(firstChar):
	from sqlalchemy import desc
	a = db.session.query(Album.by, Album.img, Album.title).distinct(Album.by).order_by(desc(Album.by)).all()
	res=[]
	for album in a:
		if album[0] != None and album[0][0].lower() == firstChar.lower():
				res.append((str(album[1]), str(album[2]), transformer(str(album[0]))))
	return res
"""
return the 10 artists corresponding the page givent, and a boolean to know if a next page is possible
True = next page available
False = the next page will be empty
"""
def getDixArtistes(firstChar, page):
	l = getArtistesByString(firstChar)
	i =((page-1)*10)+1
	j = (page*10)+1
	return (l[((page-1)*10):(page*10)+1],len(l)>=(page*10)+1)

def getAlbumsDixArtistes(firstChar, page):
	albums = getAlbums(firstChar)
	a = getDixArtistes(firstChar, page)[0]
	res = []
	for artiste in a:
		for album in albums:
			if album[2] == artiste:
				res.append(album)
	return res

def getPrecedente(page):
	if page == 1:
		return (0,False)
	else:
		return (page-1,True)
"""
return le nombre de pages possible pour le 1er char d'un artiste
"""
def getNombrePages(firstChar):
	return (len(getArtistesByString(firstChar))//10)+1
"""
return tous les premiers caractères disponibles dans la base de données
"""
def getPremiersChar():
	a = db.session.query(Album.by).distinct(Album.by).all()
	res=[]
	for artiste in a:
		if artiste[0]!=None:
			char = artiste[0][0]
			if char == '.':
				continue
		else:
			continue
		if char.upper() not in res:
			# usefull to reduce the performances problems
			char = transformer(char)
			if char.upper() not in res:
				if len(getArtistesByString(char)) > 1:
					res.append(char)
	try:
		return sorted(res)
	except:
		return res
"""
transforme l'unicode en str printable, la chaine donnée en paramètre est transformée si elle contient de l'unicode
"""
def transformer(chaine):
	import unicodedata
	#decode uses utf8 by default, must be changed if we change the coding of the text
	return chaine.encode('ascii','ignore').decode()
"""
return une liste de 3 tuples d'images au hasard dans les images de la base de données
il y a au total 9 images représentées comme-ci dessous
[(img1,img2,img3),(img4,img5,img6),(img7,img8,img9)]
"""
def getRandomImages():
	import random
	a = db.session.query(Album.img).distinct(Album.img).all()
	l = len(a)-1
	res = []
	for i in range(0,3,1):
		res.append([])
		for j in range(0,3,1):
			res[i].append(a[random.randint(1,l)][0])
	return res
"""
return tous les entryId des albums pour un genre donné,
ce resultat est de type liste sqlalchemy
"""
def getEntryIdByGenre(genre):
	from sqlalchemy import func
	return db.session.query(func.lower(Genre.genre), Genre.entryId).filter(func.lower(Genre.genre) == genre.lower()).all()
"""
return l'album qui coresspon à l'id donné
le type du resultat est une liste de type sqlalchemy
"""
def getAlbumByEntryId(entryId):
	return db.session.query(Album.by, Album.img, Album.title).filter(Album.entryId == entryId).all()
"""
return tous les albums pour un genre donée, sous forme de liste
return [(by, img, title),(by1, img1, title1),...]
"""
def getAlbumsByGenre(genre):
	g = getEntryIdByGenre(genre)
	r=[]
	for gen, entryId in g:
		dbobject = getAlbumByEntryId(entryId)
		if dbobject[0] != None:
			r.append((dbobject[0][0],dbobject[0][1],dbobject[0][2]))
	res = []
	# Obligé de faire ça pour pouvoir sort sinon None
	for a,b,c in r:
		if type(a) == type(None):
			a=""
		if type(b) == type(None):
			b=""
		if type(c) == type(None):
			c=""
		res.append((a,b,c))
	return sorted(res)
"""
return les artistes qui coresspondent aux albums donnés en paramètre sous forme de liste
"""
def getArtistesCorrespondingAlbums(albums):
	res = []
	for album in albums:
		if album[0] not in res:
			res.append(album[0])
	return res
"""
return dix artistes pour un genre et une page donnée sous forme de liste,
et un boolean qui indique si une page suivante est possible
return (["artiste",...], True)
"""
def getDixArtistesGenre(genre, page):
	albums = getAlbumsByGenre(genre)
	l = getArtistesCorrespondingAlbums(albums)
	a = ((page-1)*10)+1
	return (l[((page-1)*10):(page*10)+1],len(l)>=(page*10)+1)
"""
return une liste d'album pour dix artistes, album(image, album, groupe)
"""
def getAlbumsDixArtistesGenre(genre, page):
	albums = getAlbumsByGenre(genre)
	a = getDixArtistesGenre(genre, page)
	res = []
	for artiste in a[0]:
		for album in albums:
			if album[0] == artiste:
				res.append((album[1], album[2], album[0]))
	return res
"""
return une liste des artistes par genre (juste le nom des artsites)
"""
def getArtistesByGenre(genre):
	res = []
	g = getAlbumsByGenre(genre)
	for by,img,title in g:
		if by not in g:
			res.append(by)
	return res
"""
return une liste de genres pour un name et une image donnes
"""
def getGenresByNameAndImg(name, img):
	id = getAlbumByNameAndImg(name, img)[0][3]
	return db.session.query(Genre.genre).filter(Genre.entryId == id).all()
"""
return les informations sur un album pour un nom et une image donnes
"""
def getAlbumByNameAndImg(name, img):
	return db.session.query(Album.by, Album.img, Album.title, Album.entryId, Album.releaseYear).filter(Album.title == name and Album.img == img).all()
"""
return le nombre de page possibles pour un genre donné
"""
def getNombrePagesGenre(genre):
	return (len(getArtistesByGenre(genre))//10)
"""
return une liste de tuple(by,img, title), coresspondant au mot clé recherché
la liste est vide si il n'y a pas de resultat pour le mot clé
cette fonction ignore la case
les recherches ce font sur la date de réalisation, le nom d'artiste, le tite de l'album, et le genre.
"""
def rechercher(mot_cle,titreBool,artisteBool,genreBool,anneeBool):
	from sqlalchemy import func
	res,ids = [],set()
	# recherche par titre d'alblbum
	if titreBool:
		title = db.session.query(Album.entryId, func.lower(Album.title)).filter(func.lower(Album.title).contains(mot_cle.lower())).all()
		for entryId,titre in title:
			if entryId in ids:
				ids.add(entryId)
	# recherche par artiste
	if artisteBool:
		artiste = db.session.query(func.lower(Album.by), Album.entryId).filter(func.lower(Album.by).contains(mot_cle.lower())).all()
		for by,entryId in artiste:
			if entryId not in ids:
				ids.add(entryId)
	genre = db.session.query(func.lower(Genre.genre), Genre.entryId).filter(func.lower(Genre.genre).contains(mot_cle.lower())).all()
	# recherche par genre
	if genreBool:
		for genre, entryId in genre:
			if entryId not in ids:
				ids.add(entryId)
		releaseYear = db.session.query(Album.entryId).filter(Album.releaseYear.contains(mot_cle)).all()
	# recherche par annee
	if anneeBool:
		for entryId in releaseYear:
			if entryId not in ids:
				ids.add(entryId)
	for chaque_id in ids:
		albums = getAlbumByEntryId(chaque_id)
		for by,img,title in albums:
			tupleImgTitleBy = (img,title,by)
			if tupleImgTitleBy not in res:
				res.append(tupleImgTitleBy)
	return res